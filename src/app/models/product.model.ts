export class ProductModel {
    _id: string;
    prod_name: string = '';
    brand_name: string = '';
    barcode: number;
}
