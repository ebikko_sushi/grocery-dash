import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import {
    MatButtonModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule
} from '@angular/material';
import { ProductComponent } from './product/product.component';
import { ProductRoutingModule } from './product-routing.module';

@NgModule({
    declarations: [
        ProductComponent
    ],
    imports: [
        CommonModule,
        ProductRoutingModule,
        FormsModule,
        MatButtonModule,
        MatInputModule,
        MatPaginatorModule,
        MatSortModule,
        MatTableModule
    ]
})
export class ProductModule { }
