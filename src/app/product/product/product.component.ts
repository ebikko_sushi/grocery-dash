import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

import { DataService } from '../../services/data.service';
import { ProductModel } from '../../models/product.model';

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

    productsDataSource: any;
    productColumns: Array<string> = ['prod_name', 'brand_name', 'barcode', 'actions'];
    table: any = {
        length: 0
    }
    state: string = 'main-display';
    tempProduct: ProductModel;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(
        private dataService: DataService
    ) {
        this.initTable();
    }

    ngOnInit() {
    }

    initTable() {
        this.dataService.readAllProducts()
            .subscribe(result => {
                if (result.success) {
                    this.table.length = result.count;
                    this.productsDataSource = new MatTableDataSource(result.items);
                    setTimeout(() => {
                        this.productsDataSource.paginator = this.paginator;
                        this.productsDataSource.sort = this.sort;
                    }, 0);
                }
            }, err => {
                alert(err.error.message);
            });
    }

    openAddForm() {
        this.tempProduct = new ProductModel();
        this.state = 'add-form';
    }

    openEditForm(product) {
        this.tempProduct = product;
        this.state = 'edit-form';
    }

    createProduct() {
        this.dataService.createProduct(this.tempProduct)
            .subscribe(result => {
                if (result.success) {
                    this.initTable();
                    this.state = 'main-display';
                }
            }, err => {
                alert(err.error.message);
            });
    }

    updateProduct() {
        this.dataService.updateProductById(this.tempProduct._id, this.tempProduct)
            .subscribe(result => {
                if (result.success) {
                    this.initTable();
                    this.state = 'main-display';
                }
            }, err => {
                alert(err.error.message);
            });
    }

    deleteProduct(id) {
        this.dataService.deleteProductById(id)
            .subscribe(result => {
                if (result.success) {
                    this.initTable();
                    this.state = 'main-display';
                }
            }, err => {
                alert(err.error.message);
            });
    }
}
