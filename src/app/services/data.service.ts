import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';

const apiUrl = environment.apiUrl;

@Injectable({
    providedIn: 'root'
})
export class DataService {

    constructor(
        private http: HttpClient
    ) { }

    createProduct(product): Observable<any> {
        return this.http.post(`${apiUrl}/products`, product)
            .pipe(
                tap(result => {
                    console.log('createProduct() successful', result);
                }),
                catchError(this.handleError('createProduct'))
            );
    }

    readAllProducts(): Observable<any> {
        return this.http.get(`${apiUrl}/products`)
            .pipe(
                tap(result => {
                    console.log('readAllProducts() successful', result);
                }),
                catchError(this.handleError('readAllProducts'))
            );
    }

    readProductById(id): Observable<any> {
        return this.http.get(`${apiUrl}/products/${id}`)
            .pipe(
                tap(result => {
                    console.log('readProductById() successful', result);
                }),
                catchError(this.handleError('readProductById'))
            );
    }

    updateProductById(id, product): Observable<any> {
        return this.http.put(`${apiUrl}/products/${id}`, product)
            .pipe(
                tap(result => {
                    console.log('updateProductById() successful', result);
                }),
                catchError(this.handleError('updateProductById'))
            );
    }

    deleteProductById(id): Observable<any> {
        return this.http.delete(`${apiUrl}/products/${id}`)
            .pipe(
                tap(result => {
                    console.log('deleteProductById() successful', result);
                }),
                catchError(this.handleError('deleteProductById'))
            );
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.log(`err in ${operation}:`, error);
            return throwError(error);
        }
    }
}
